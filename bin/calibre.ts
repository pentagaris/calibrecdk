import { App } from 'aws-cdk-lib';
import { PipelineStack } from '../lib/stacks/pipeline-stack';

const app = new App();
new PipelineStack(app, 'CalibrePipelineStack')
