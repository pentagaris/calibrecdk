import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as basic from '../constructs/basic';
import * as calibre from '../constructs/calibre';
import * as secretsManager from 'aws-cdk-lib/aws-secretsmanager';

export class CalibreStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const secret = new secretsManager.Secret(this, 'CalibrePassword', {
      description: 'Calibre User Password',
      generateSecretString: {
        excludePunctuation: true,
        passwordLength: 20,
      }
    });

    const loggingBucket = new basic.LoggingBucket(this, 'CalibreLogs', {});
    const bucket = new basic.SecureBucket(this, 'CalibreBucket', { application: 'Calibre', loggingBucket: loggingBucket }).bucket;
    const logGroup = new basic.CloudwatchGroup(this, 'CalibreASGLogGroup', {
      namespace: 'Calibre',
      service: 'ASGInstances',
    })

    new calibre.ASG(this, 'CalibreASG', {
      vpc: new calibre.IngressVpc(this, 'CalibreVpc'),
      bucket: bucket,
      secret: secret,
      logGroup: logGroup,
    });
  }
}
