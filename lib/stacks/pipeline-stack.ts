import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { CalibrePipelineStage } from './pipeline-stage';
import * as codecommit from 'aws-cdk-lib/aws-codecommit';
import * as pipelines from 'aws-cdk-lib/pipelines'

export class PipelineStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // This is a comment to force mirroring
    const repo = new codecommit.Repository(this, 'CalibreRepo', { repositoryName: "CalibreCDK" });

    const pipeline = new pipelines.CodePipeline(this, 'Pipeline', {
      pipelineName: 'CalibrePipeline',
      synth: new pipelines.CodeBuildStep('SynthStep', {
        input: pipelines.CodePipelineSource.codeCommit(repo, 'main'),
        installCommands: [
          'npm install -g aws-cdk'
        ],
        commands: [
          'npm ci',
          'npm run build',
          'npx cdk synth',
        ]
      })
    });
    const deploy = new CalibrePipelineStage(this, 'Deploy');
    const deployStage = pipeline.addStage(deploy);
  }
}

