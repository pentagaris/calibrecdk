import { Construct } from "constructs";
import * as s3 from "aws-cdk-lib/aws-s3";
import * as iam from "aws-cdk-lib/aws-iam";
import * as ec2 from "aws-cdk-lib/aws-ec2";
import { LogGroup, RetentionDays } from "aws-cdk-lib/aws-logs";
import { HostedZone } from "aws-cdk-lib/aws-route53";
import { PolicyStatement } from "aws-cdk-lib/aws-iam";
import { RemovalPolicy } from "aws-cdk-lib";

/** Props which are being passed to our Bucket construct **/
export interface SecureBucketProps {
  readonly loggingBucket: LoggingBucket;
  readonly deleteBucket?: boolean;
  readonly application: string;
}

export interface LoggingBucketProps {
  readonly deleteBucket?: boolean;
}

export interface CloudwatchGroupProps {
  readonly service: string;
  readonly namespace: string;
}

export interface FileCollectList {
  log_group_name: string;
  file_path: string;
  log_stream_name: string;
  timestamp_format?: string;
}

export interface EC2CloudwatchInitConfigProps {
  readonly cloudwatchLogGroupProps: CloudwatchGroupProps;
}

export interface LetsEncryptLambdaProps {
  readonly hostedZone: HostedZone;
  readonly domain: string;
}

/** Logging Bucket with Sane Defaults. **/
export class LoggingBucket extends Construct {
  public readonly bucket: s3.Bucket;

  constructor(scope: Construct, id: string, props: LoggingBucketProps) {
    super(scope, id);

    const deleteBucket = props.deleteBucket ?? true;

    this.bucket = new s3.Bucket(this, "LoggingBucket", {
      versioned: true,
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
      objectOwnership: s3.ObjectOwnership.BUCKET_OWNER_PREFERRED,
      removalPolicy: deleteBucket
        ? RemovalPolicy.DESTROY
        : RemovalPolicy.RETAIN,
      autoDeleteObjects: deleteBucket,
      enforceSSL: true,
    });
    this.bucket.addToResourcePolicy(
      new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        principals: [new iam.ServicePrincipal("logging.s3.amazonaws.com")],
        actions: ["s3:PutObject"],
        resources: [`${this.bucket.bucketArn}/*`],
      })
    );
  }
}

/** Bucket with Secure Defaults. Access is governed by IAM Policies **/
export class SecureBucket extends Construct {
  public readonly bucket: s3.Bucket;

  constructor(scope: Construct, id: string, props: SecureBucketProps) {
    super(scope, id);

    const loggingBucket = props.loggingBucket;
    const deleteBucket = props.deleteBucket ?? true;

    this.bucket = new s3.Bucket(this, "SecureBucket", {
      versioned: true,
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
      serverAccessLogsBucket: loggingBucket.bucket,
      serverAccessLogsPrefix: `s3 / ${props.application} / `,
      enforceSSL: true,
      objectOwnership: s3.ObjectOwnership.BUCKET_OWNER_ENFORCED,
      removalPolicy: deleteBucket
        ? RemovalPolicy.DESTROY
        : RemovalPolicy.RETAIN,
      autoDeleteObjects: deleteBucket,
    });
  }
}

/** Construct for how I like to build Cloudwatch Groups **/
export class CloudwatchGroup extends Construct {
  public readonly logGroup: LogGroup;
  public readonly namespace: string;
  public readonly service: string;

  constructor(scope: Construct, id: string, props: CloudwatchGroupProps) {
    super(scope, id);
    this.namespace = `/${props.namespace}`;
    this.service = `/${props.service}`;

    this.logGroup = new LogGroup(this, "LogGroup", {
      logGroupName: this.namespace.concat(this.service),
      retention: RetentionDays.TEN_YEARS,
      removalPolicy: RemovalPolicy.DESTROY,
    });
  }

  createInstanceCloudwatchConfig(files?: FileCollectList[]): ec2.InitConfig {
    const filesToCollect: FileCollectList[] = [
      {
        log_group_name: this.logGroup.logGroupName,
        file_path:
          "/var/log/amazon/amazon-cloudwatch-agent/amazon-cloudwatch-agent.log",
        log_stream_name:
          "{instance_id}/var/log/amazon/amazon-cloudwatch-agent/amazon-cloudwatch-agent.log",
        timestamp_format: "%Y-%m-%dT%H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path:
          "/var/log/amazon/amazon-cloudwatch-agent/configuration-validation.log",
        log_stream_name:
          "{instance_id}/var/log/amazon/amazon-cloudwatch-agent/configuration-validation.log",
        timestamp_format: "%Y/%m/%d %H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/amazon/ssm/amazon-ssm-agent.log",
        log_stream_name:
          "{instance_id}/var/log/amazon/ssm/amazon-ssm-agent.log",
        timestamp_format: "%Y-%m-%d %H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/amazon/ssm/errors.log",
        log_stream_name: "{instance_id}/var/log/amazon/ssm/errors.log",
        timestamp_format: "%Y-%m-%d %H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/audit/audit.log",
        log_stream_name: "{instance_id}/var/log/audit/audit.log",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/boot.log",
        log_stream_name: "{instance_id}/var/log/boot.log",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/cfn-hup.log",
        log_stream_name: "{instance_id}/var/log/cfn-hup.log",
        timestamp_format: "%Y-%m-%d %H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/cfn-init-cmd.log",
        log_stream_name: "{instance_id}/var/log/cfn-init-cmd.log",
        timestamp_format: "%Y-%m-%d %H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/cfn-init.log",
        log_stream_name: "{instance_id}/var/log/cfn-init.log",
        timestamp_format: "%Y-%m-%d %H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/cfn-wire.log",
        log_stream_name: "{instance_id}/var/log/cfn-wire.log",
        timestamp_format: "%Y-%m-%d %H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/cloud-init-output.log",
        log_stream_name: "{instance_id}/var/log/cloud-init-output.log",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/cloud-init.log",
        log_stream_name: "{instance_id}/var/log/cloud-init.log",
        timestamp_format: "%b %d %H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/cron",
        log_stream_name: "{instance_id}/var/log/cron",
        timestamp_format: "%b %-d %H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/dmesg",
        log_stream_name: "{instance_id}/var/log/dmesg",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/grubby_prune_debug",
        log_stream_name: "{instance_id}/var/log/grubby_prune_debug",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/maillog",
        log_stream_name: "{instance_id}/var/log/maillog",
        timestamp_format: "%b %-d %H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/messages",
        log_stream_name: "{instance_id}/var/log/messages",
        timestamp_format: "%b %-d %H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/secure",
        log_stream_name: "{instance_id}/var/log/secure",
        timestamp_format: "%b %-d %H:%M:%S",
      },
      {
        log_group_name: this.logGroup.logGroupName,
        file_path: "/var/log/yum.log",
        log_stream_name: "{instance_id}/var/log/yum.log",
        timestamp_format: "%b %d %H:%M:%S",
      },
    ];

    if (files && files.length) {
      filesToCollect.concat(files);
    }

    const logFileCollectList = {
      logs: {
        logs_collected: {
          files: {
            collect_list: filesToCollect,
          },
        },
      },
    };

    const handle = new ec2.InitServiceRestartHandle();
    return new ec2.InitConfig([
      ec2.InitFile.fromObject(
        "/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.d/files.json",
        logFileCollectList,
        { serviceRestartHandles: [handle] }
      ),
      ec2.InitPackage.yum("amazon-cloudwatch-agent"),
      ec2.InitService.enable("amazon-cloudwatch-agent", {
        serviceRestartHandle: handle,
      }),
    ]);
  }

  createCloudwatchPolicy() {
    const logsPolicy = new iam.ManagedPolicy(
      this,
      "CalibreInstanceLogsPolicy",
      {
        description:
          "Policy which allows logs to be pushed to this cloudwatch group",
        statements: [
          new iam.PolicyStatement({
            effect: iam.Effect.ALLOW,
            resources: ["*"],
            actions: ["Cloudwatch:PutMetricData"],
            conditions: {
              StringEquals: { "cloudwatch:namespace": this.namespace },
            },
          }),
          new iam.PolicyStatement({
            effect: iam.Effect.ALLOW,
            resources: [this.logGroup.logGroupArn],
            actions: [
              "logs:CreateLogGroup",
              "logs:CreateLogStream",
              "logs:PutLogEvents",
              "logs:DescribeLogStreams",
              "logs:DescribeLogGroups",
            ],
          }),
          new iam.PolicyStatement({
            effect: iam.Effect.ALLOW,
            resources: ["*"],
            actions: ["ec2:DescribeTags"],
          }),
        ],
      }
    );
    return logsPolicy;
  }
  createFileFailureAlarm() {
    console.log("WIP");
  }
}

/** Construct for a LetsEncrypt Lambda 
TODO: Implement
**/
export class LetsEncryptLambda extends Construct {
  constructor(scope: Construct, id: string, props: LetsEncryptLambdaProps) {
    super(scope, id);

    const letsEncryptPolicy = new iam.ManagedPolicy(
      this,
      "Route53UpdatePolicy",
      {
        description:
          "Policy which allows for manipulation of a Route53 Hosted Zone",
        statements: [new iam.PolicyStatement({})],
      }
    );
  }
}
