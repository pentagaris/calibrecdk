import * as cdk from "aws-cdk-lib";
import { Construct } from "constructs";
import * as ec2 from "aws-cdk-lib/aws-ec2";
import * as secretsManager from "aws-cdk-lib/aws-secretsmanager";
import * as iam from "aws-cdk-lib/aws-iam";
import * as s3 from "aws-cdk-lib/aws-s3";
import * as basic from "../constructs/basic";
import * as autoscaling from "aws-cdk-lib/aws-autoscaling";
// import { HostedZone } from "aws-cdk-lib/aws-route53";

/** Props which we are passing to our instance construct **/
export interface ASGProps {
  readonly vpc: IngressVpc;
  readonly logGroup: basic.CloudwatchGroup;
  readonly secret: secretsManager.Secret;
  readonly bucket: s3.Bucket;
}

/** Props which are being passed to our Role construct **/
export interface RoleProps {
  readonly bucket: s3.Bucket;
  readonly secret: secretsManager.Secret;
  readonly logGroup: basic.CloudwatchGroup;
}

export class CalibreRole extends Construct {
  public readonly role: iam.Role;

  constructor(scope: Construct, id: string, props: RoleProps) {
    super(scope, id);

    const bucketPolicy = new iam.ManagedPolicy(this, "CalibreBucketPolicy", {
      description:
        "Policy which allows bi-directional sync to a specified bucket",
      statements: [
        new iam.PolicyStatement({
          actions: ["s3:ListBucket"],
          effect: iam.Effect.ALLOW,
          resources: [props.bucket.bucketArn],
        }),
        new iam.PolicyStatement({
          actions: ["s3:GetObject*", "s3:PutObject*"],
          effect: iam.Effect.ALLOW,
          resources: [props.bucket.arnForObjects("*")],
        }),
      ],
    });
    const secretPolicy = new iam.ManagedPolicy(this, "CalibreSecretPolicy", {
      description:
        "Policy which allows the calibre instance to retrieve the user password",
      statements: [
        new iam.PolicyStatement({
          actions: ["secretsmanager:GetSecretValue"],
          effect: iam.Effect.ALLOW,
          resources: [props.secret.secretArn],
        }),
        new iam.PolicyStatement({
          actions: ["ec2:DescribeTags"],
          effect: iam.Effect.ALLOW,
          resources: ["*"],
        }),
      ],
    });
    this.role = new iam.Role(this, "CalibreRole", {
      assumedBy: new iam.ServicePrincipal("ec2.amazonaws.com"),
      description:
        "Role which allows bi-directional sync to a specified bucket",
    });

    this.role.addManagedPolicy(bucketPolicy);
    this.role.addManagedPolicy(secretPolicy);
    this.role.addManagedPolicy(props.logGroup.createCloudwatchPolicy());
    this.role.addManagedPolicy(
      iam.ManagedPolicy.fromAwsManagedPolicyName("AmazonSSMManagedInstanceCore")
    );
  }
}

export class IngressVpc extends Construct {
  public readonly vpc: ec2.Vpc;
  public readonly securityGroups: Record<string, ec2.ISecurityGroup> = {};

  constructor(scope: Construct, id: string) {
    super(scope, id);

    const logGroup = new basic.CloudwatchGroup(this, "CalibreLogGroup", {
      namespace: "Calibre",
      service: "VPCFlow",
    });

    this.vpc = new ec2.Vpc(this, "Vpc", {
      maxAzs: 1, //Only use 1 AZ for this VPC so we can do reserved instances
      subnetConfiguration: [
        {
          subnetType: ec2.SubnetType.PUBLIC,
          cidrMask: 28,
          name: "CalibreIngressSubnet",
        },
      ],
      flowLogs: {
        ["flow"]: {
          destination: ec2.FlowLogDestination.toCloudWatchLogs(
            logGroup.logGroup
          ),
          trafficType: ec2.FlowLogTrafficType.ALL,
        },
      },
    });

    /** Security Group which allows ingress to Calibre **/
    const securityGroup = new ec2.SecurityGroup(this, "CalibreSecurityGroup", {
      vpc: this.vpc,
      securityGroupName: "calibreSecurityGroup",
      description: "Security group to allow ingress to Calibre",
    });
    securityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(80));
    securityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(443));

    this.securityGroups.calibreSecurityGroup = securityGroup;

    /** Tags for this VPC **/
    cdk.Tags.of(this.vpc).add("applicationVpc", "Calibre");
  }
}

/** 
Creates an Instance with Calibre installed as part of the initialization.
Also generates a password for the default user.
**/
export class ASG extends Construct {
  public readonly asg: autoscaling.AutoScalingGroup;
  constructor(scope: Construct, id: string, props: ASGProps) {
    super(scope, id);

    this.asg = new autoscaling.AutoScalingGroup(this, "CaliberASG", {
      vpc: props.vpc.vpc,
      securityGroup: props.vpc.securityGroups.calibreSecurityGroup,
      instanceType: ec2.InstanceType.of(
        ec2.InstanceClass.T3,
        ec2.InstanceSize.MICRO
      ),
      machineImage: new ec2.AmazonLinuxImage({
        generation: ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
      }),
      role: new CalibreRole(this, "CalibreRoleConstruct", {
        bucket: props.bucket,
        secret: props.secret,
        logGroup: props.logGroup,
      }).role,
      desiredCapacity: 1,
      minCapacity: 1,
      maxCapacity: 1,
      updatePolicy: autoscaling.UpdatePolicy.replacingUpdate(),
      init: ec2.CloudFormationInit.fromConfigSets({
        configSets: {
          default: [
            "yumPreinstall",
            "logsConfig",
            "dockerConfig",
            "route53Config",
            "calibreConfig",
          ],
        },
        configs: {
          yumPreinstall: new ec2.InitConfig([
            ec2.InitPackage.yum("git"),
            ec2.InitPackage.yum("tmux"),
            ec2.InitPackage.yum("docker"),
            ec2.InitPackage.yum("jq"),
          ]),
          logsConfig: props.logGroup.createInstanceCloudwatchConfig(),
          dockerConfig: new ec2.InitConfig([ec2.InitService.enable("docker")]),
          route53Config: new ec2.InitConfig([
            ec2.InitFile.fromAsset(
              "/tmp/update_route53.py",
              "assets/scripts/update_route53.py"
            ),
          ]),
          calibreConfig: new ec2.InitConfig([
            ec2.InitCommand.shellCommand(
              "mkdir -p /srv/calibre/library /srv/calibre/toadd",
              {
                key: "00MakeCalibreDir",
                // ignoreErrors: true
              }
            ),
            ec2.InitCommand.shellCommand("touch /srv/calibre/calibre.env", {
              key: "01MakeCalibreFiles",
              // ignoreErrors: true
            }),
            ec2.InitCommand.shellCommand(
              'echo "PUID=1001\nPGID=1001\nTZ=America/Denver\n" > /srv/calibre/calibre.env',
              {
                key: "02SetEnvVars",
                // ignoreErrors: true
              }
            ),
            ec2.InitCommand.shellCommand(
              `aws s3 sync /srv/calibre/ s3://${props.bucket.bucketName}/ --region ${cdk.Aws.REGION} 1> /dev/null`,
              {
                key: "03InitCalibreFoldersS3",
                // ignoreErrors: true,
              }
            ),
            ec2.InitCommand.shellCommand(
              `aws s3 sync s3://${props.bucket.bucketName} /srv/calibre/ --region ${cdk.Aws.REGION} 1> /dev/null`,
              {
                key: "04GetCalibreLibrary",
                // ignoreErrors: true,
              }
            ),
            ec2.InitCommand.shellCommand(
              `aws s3 sync s3://${props.bucket.bucketName}/toadd /srv/calibre/toadd --region ${cdk.Aws.REGION} 1> /dev/null`,
              {
                key: "05GetCalibreToadd",
                // ignoreErrors: true,
              }
            ),
            ec2.InitCommand.shellCommand(
              `echo "* * * * * root /usr/bin/flock -xn /var/lock/syncdown.lock -c '/usr/bin/aws s3 sync s3://${props.bucket.bucketName}/library/ /srv/calibre/library --region ${cdk.Aws.REGION} 2>> /var/log/syncdown.log'" >> /etc/crontab`,
              {
                key: "06SetSyncDown",
                // ignoreErrors: true
              }
            ),
            ec2.InitCommand.shellCommand(
              `echo "* * * * * root /usr/bin/flock -xn /var/lock/syncup.lock -c '/usr/bin/aws s3 sync /srv/calibre/library s3://${props.bucket.bucketName}/library/ --region ${cdk.Aws.REGION} 2>> /var/log/syncup.log'" >> /etc/crontab`,
              {
                key: "07SetSyncUp",
                // ignoreErrors: true
              }
            ),
            ec2.InitCommand.shellCommand(
              "curl -L https://www.gutenberg.org/ebooks/345.epub.images --output /srv/calibre/toadd/dracula.epub",
              {
                key: "08GetDracula",
                // ignoreErrors: true
              }
            ),
            ec2.InitCommand.shellCommand(
              "docker run -d --rm --env-file /srv/calibre/calibre.env --volume /srv/calibre/:/calibre --entrypoint /usr/bin/calibredb lscr.io/linuxserver/calibre --with-library=/calibre/library add -m ignore -r /calibre/toadd/",
              {
                key: "09InitCalibreLibrary",
                // ignoreErrors: true,
              }
            ),
            ec2.InitCommand.shellCommand(
              `docker run -d --rm --env-file /srv/calibre/calibre.env --volume /srv/calibre:/calibre --entrypoint /usr/bin/calibre-server lscr.io/linuxserver/calibre --userdb=/calibre/users.db --manage-users -- add calibre $(/usr/bin/aws secretsmanager get-secret-value --secret-id ${props.secret.secretArn} --region ${cdk.Aws.REGION} | jq -r .SecretString)`,
              {
                key: "10InitCalibreUser",
                // ignoreErrors: true,
              }
            ),
            ec2.InitCommand.shellCommand(
              "docker run -d --restart unless-stopped --name=calibre --env-file=/srv/calibre/calibre.env -p 80:80 --volume /srv/calibre/:/calibre --entrypoint /usr/bin/calibre-server lscr.io/linuxserver/calibre --port 80 --enable-auth --userdb /calibre/users.db /calibre/library",
              {
                key: "11StartCalibre",
                // ignoreErrors: true,
              }
            ),
            ec2.InitCommand.shellCommand(
              "/usr/bin/python3 /tmp/update_route53.py",
              {
                key: "12CreateDomainData",
                // ignoreErrors: true,
              }
            ),
          ]),
        },
      }),
      initOptions: { configSets: ["default"], includeUrl: true },
      signals: autoscaling.Signals.waitForMinCapacity({
        timeout: cdk.Duration.minutes(20),
      }),
      blockDevices: [
        {
          deviceName: "/dev/xvda",
          volume: autoscaling.BlockDeviceVolume.ebs(30),
        },
      ],
    });
    cdk.Tags.of(this.asg).add("CalibrePassword", props.secret.secretArn);
  }
}
