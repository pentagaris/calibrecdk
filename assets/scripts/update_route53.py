import json 
import sys
from urllib.request import Request, urlopen

with open('/run/cloud-init/instance-data.json') as infile:
    metadata = json.load(infile)
    pubip = metadata["ds"]["meta-data"]["public-ipv4"]
    data = json.dumps({"target_url": "books.djmc.info", "hosted_zone": "Z1PX89Q7PYDP3X", "ip_address": pubip}).encode('utf-8')
    headers = {"x-api-key": "Ljw0xQnbYA6iuN9VULtnq9dMUJD89fm35Zae1MQo", "Content-Type": "application/json"}
    request = Request(url="https://kvs9is7goa.execute-api.us-east-1.amazonaws.com/Prod", data=data, headers=headers, method='POST')
    response = urlopen(request)
    if response.code >= 400:
        sys.exit(2)
    else:
        sys.exit(0)
