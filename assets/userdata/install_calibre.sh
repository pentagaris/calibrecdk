#!/bin/bash
yum upgrade -y
yum install flatpak jq tmux docker -y

systemctl enable docker --now

docker run 

TAG_NAME="CalibrePassword"
INSTANCE_ID="$(wget -qO- http://instance-data/latest/dynamic/instance-identity/document | jq -r .instanceId)"
REGION="$(wget -qO- http://instance-data/latest/dynamic/instance-identity/document | jq -r .region)"
TAG_VALUE="$(aws ec2 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=$TAG_NAME" --region $REGION --output=json | jq .Tags[0].Value)"
PASSWORD=$(aws secretsmanager get-secret-value --secret-id $TAG_VALUE)
PUBLIC_IP=$(wget -qO- http://instance-data/latest/meta-data/public-ipv4)

useradd -m calibre
su calibre -c "mkdir -p /home/calibre/library"
su calibre -c "touch /home/calibre/calibre-server.log"
su calibre -c "curl -L https://www.gutenberg.org/ebooks/345.epub.images --output /home/calibre/dracula.epub"
su calibre -c "flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo --user"
su calibre -c "flatpak install flathub com.calibre_ebook.calibre -y --user"
su calibre -c "flatpak run --command='calibredb' com.calibre_ebook.calibre add --with-library=/home/calibre/library /home/calibre/dracula.epub"
su calibre -c "flatpak run --command='calibre-server' com.calibre_ebook.calibre --userdb /home/calibre/users.db --manage-users add calibre $PASSWORD"
su calibre -c "flatpak run --command='calibre-server' com.calibre_ebook.calibre --userdb /home/calibre/users.db --enable-auth --log=/home/calibre/calibre-server.log --daemonize /home/calibre/library --listen-on '127.0.0.1'"
